<?php

namespace App\Email;
use App\Message\Message;
use App\Model\database as db;
use App\Utility\Utility;

//require_once("../../../../vendor/autoload.php");




class Email extends db{
    public $id;
    public $name;
    public $email_id;
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data = Null)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];

        }
        if (array_key_exists('name', $data)) {
            $this->name= $data['name'];

        }
        if (array_key_exists('email_id', $data)) {
            $this->email_id= $data['email_id'];

        }

    }
    public function store(){
        $arrData=array($this->name,$this->email_id);

        $sql= "Insert INTO email(name,email_id) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("Sucess!data has been inserted sucessfully");
        else
            Message::setMessage("Failure!data has not been inserted sucessfully");

        Utility::redirect('create.php');
    }// end of store method
}

//$objEmail=new Email();

