<?php

namespace App\Hobbies;
use App\Message\Message;
use App\Model\database as db;
use App\Utility\Utility;

//require_once("../../../../vendor/autoload.php");




class Hobbies extends db{
    public $id;
    public $name;
    public $hobby;
    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data = Null)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];

        }
        if (array_key_exists('name', $data)) {
            $this->name= $data['name'];

        }
        if (array_key_exists('hobby', $data)) {
            $this->hobby =implode(",", $data['hobby']);

        }

    }
    public function store(){
        $arrData=array($this->name,$this->hobby);

        $sql= "Insert INTO hobbies(name,hobby) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql);

        $result= $STH->execute($arrData);

        if($result)
            Message::setMessage("Sucess!data has been inserted sucessfully");
        else
            Message::setMessage("Failure!data has not been inserted sucessfully");

        Utility::redirect('create.php');
    }// end of store method
}
//$objHobbies=new Hobbies();
